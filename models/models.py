import random

from mongoengine import DynamicDocument, StringField, FloatField, ListField, MultiPointField, LineStringField, \
    PointField, EmbeddedDocument, EmbeddedDocumentListField, EmbeddedDocumentField


class Location(EmbeddedDocument):
    name = StringField()
    loc = MultiPointField(auto_index=True)


class Campaign(DynamicDocument):
    name = StringField()
    price = FloatField()
    ad_creative = StringField(
        default=f'''
                <html>
                    <head>
                    This is the head of your page
                    <title>Example HTML page</title>
                    </head>
                    <body>
                    This is the body of your page.
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-1234567890123456" crossorigin="anonymous"></script>
                        <!-- Homepage Leaderboard -->
                        <ins class="adsbygoogle"
                        style="display:inline-block;width:728px;height:90px"
                        data-ad-client="ca-pub-1234567890123456"
                        data-ad-slot="{random.randint(100_000, 200_000)}"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({'{}'});
                        </script>
                    </body>
                </html>'''
    )
    targetedCountries = ListField(StringField())
    targetedLocations = ListField(Location)

    @staticmethod
    def parse_payload(self, payload: dict):
        return Campaign(**payload)


class MobileApp(EmbeddedDocument):
    app_id = StringField()
    app_name = StringField()


class DeviceLocation(EmbeddedDocument):
    country = StringField()
    loc = PointField(auto_index=True)


class MobileDevice(EmbeddedDocument):
    device_id = StringField()
    device_os = StringField()
    location = EmbeddedDocumentField(DeviceLocation)


class BidRequest(DynamicDocument):
    bid_id = StringField()
    mobile_app_info = EmbeddedDocumentField(MobileApp)
    mobile_device_info = EmbeddedDocumentField(MobileDevice)


class BidResponse:
    pass
