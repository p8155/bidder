import json
import uuid
from typing import List

import requests

from config.config import get_config
from utils.utils import get_main_logger, func_name, create_result, dmsg

l = get_main_logger('bidder_logic_functions')
c = get_config()


def get_appropriate_campaigns(request_body: dict):
    try:
        response: requests.Response = requests.post(f"http://{c.get('campaign_service_url')}", data=json.dumps(request_body))
        result = json.loads(response.content)
        if result.get('status_code') != 200:
            raise Exception(dmsg('') + 'something went wrong with the campaigns request')
        return result.get('items')
    except Exception as e:
        l.exception(func_name() + ": " + e.__str__())
        return []


def get_highest_bid(campaigns: List[dict]) -> List[dict]:
    # print(campaigns)
    h_bid = None
    h_price = 0.0
    maxPricedItem = max(campaigns, key=lambda x: x['price'])
    # print("maxPricedItem: ")
    # print(maxPricedItem)
    bid_response = {
        "bid_id": str(uuid.uuid4()),
        "campaign_id": maxPricedItem.get('_id').get('$oid'),
        "price": maxPricedItem.get('price'),
        "ad_creative": maxPricedItem.get('ad_creative'),
    }
    return [bid_response]


def find_appropriate_bids(payload: dict):
    try:
        # print(payload)
        _country = payload.get('mobile_device_info').get('location').get('country', "Greece")
        _loc = payload.get('mobile_device_info').get('location').get('loc', [0, 0])
        appropriate_campaigns = get_appropriate_campaigns({"country": _country, "location": _loc})
        # print("appropriate_campaigns: ")
        # print(type(appropriate_campaigns))
        # print(len(appropriate_campaigns))
        # print(appropriate_campaigns)
        result = get_highest_bid(list(map(json.loads, appropriate_campaigns)))
        return create_result(items=[json.dumps(result)], result_=func_name() + '_SUCCESS', status_code=200)
        # return {}
    except Exception as e:
        l.exception(dmsg('') + e.__str__())
        return create_result(items=[], result_=func_name() + '_FAILURE', more=e.__str__(), status_code=500)
