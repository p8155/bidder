import unittest
import uuid
from typing import List

from logic.bidder_logic_fuctions import find_appropriate_bids, get_appropriate_campaigns


class TestCases(unittest.TestCase):
    def test_find_appropriate_bids(self):
        request_body = {
            "bid_id": str(uuid.uuid4()),
            "mobile_app_info": {
                "app_id": str(uuid.uuid4()),
                "app_name": str(uuid.uuid4())
            },
            "mobile_device_info": {
                "device_id": str(uuid.uuid4()),
                "device_os": "Android",
                "location": {
                    "country": "Greece",
                    "loc": [37.751, 27.822]
                }
            }
        }
        result1 = find_appropriate_bids(request_body)
        # print(result1)
        self.assertEqual(result1.get('status_code'), 200)
        self.assertEqual(result1.get('result'), 'FIND_APPROPRIATE_BIDS_SUCCESS')

    def test_get_appropriate_campaigns(self):
        req_body = {"country": "Greece", "location": [37.751, 27.822]}
        result = get_appropriate_campaigns(req_body)
        print(result)
        self.assertIsInstance(result, List)
