import json

from sanic import Sanic
from sanic import json as sanic_json

from logic.bidder_logic_fuctions import find_appropriate_bids
from utils.utils import get_main_logger, create_result

app = Sanic('Bidder')

log = get_main_logger(__name__)


@app.get("/health")
async def health(request):
    return sanic_json(create_result(result_="This is the Bidder Running", status_code=200))


@app.post("/bids/request")
async def bid_request(request):
    payload = json.loads(request.body)
    result = find_appropriate_bids(payload)
    return sanic_json(result, status=200)


if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)
    # print(find_appropriate_bids(country="Greece", location=[37.751, 25.144]))
